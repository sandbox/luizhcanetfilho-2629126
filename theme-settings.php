<?php
function semantic_plus_form_system_theme_settings_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state) {
  $var_files = file_scan_directory(drupal_get_path('theme', 'semantic_plus') . '/semantic/src/themes', '/\.variables$/');

  foreach ($var_files as $key => $value) {
    $key = array_reverse(explode('/', $key));
    $form['semantic_ui_themes']['#items'][$key[1]][$value->name][] = $key[2];
  }

  $themes_dir = file_scan_directory(drupal_get_path('theme', 'semantic_plus') . '/semantic/src/themes', '/.*/', array('recurse' => FALSE));

  foreach ($themes_dir as $key => $value) {
    $form['semantic_ui_all_themes']['#items'][$value->name] = t(ucwords($value->name));
  }

  array_multisort($form['semantic_ui_all_themes']['#items'], SORT_STRING);
  $form['semantic_ui_all_themes']['#items'] = array('default' => $form['semantic_ui_all_themes']['#items']['default']) + $form['semantic_ui_all_themes']['#items'];

  $form['semantic_ui_theme'] = array(
    '#type' => "details",
    '#title' => t("Semantic UI Theme"),
    '#open' => TRUE,
    '#description' => t("You can change the look and feel of Semantic UI using the bundled Semantic UI Themes."),
    'toggle_global' => array(
      '#type' => "checkbox",
      '#title' => t("Change Globally"),
      '#description' => t("Unchecking this you can change the theme for each component of Semantic UI."),
      '#default_value' => theme_get_setting('features.global'),
    ),
    'global_theme' => array(
      '#type' => "select",
      '#title' => t("Theme"),
      '#options' => $form['semantic_ui_all_themes']['#items'],
      '#default_value' => theme_get_setting('global_theme'),
      '#states' => array(
        'invisible' => array(
          'input[name="toggle_global"]' => array(
            'checked' => FALSE,
          ),
        ),
      ),
    ),
    'components_themes' => array(
      '#type' => "details",
      '#title' => t("Components Themes"),
      '#open' => TRUE,
      '#states' => array(
        'invisible' => array(
          'input[name="toggle_global"]' => array(
            'checked' => TRUE,
          ),
        ),
      ),
    ),
  );

  foreach ($form['semantic_ui_themes']['#items'] as $section => $components) {
    $form['semantic_ui_theme']['components_themes'][$section] = array(
      '#type' => "fieldset",
      '#title' => t(ucwords($section)),
    );
    ksort($components);

    foreach ($components as $component => $themes) {
      $form['semantic_ui_theme']['components_themes'][$section][$component] = array(
        '#type' => "select",
        '#title' => t(ucwords($component)),
        '#options' => array(),
        '#default_value' => theme_get_setting($component),
      );
      ksort($themes);

      foreach ($themes as $theme) {
        $form['semantic_ui_theme']['components_themes'][$section][$component]['#options'][$theme] = t(ucwords($theme));
        ksort($form['semantic_ui_theme']['components_themes'][$section][$component]['#options']);
        unset($form['semantic_ui_theme']['components_themes'][$section][$component]['#options']['default']);
        $form['semantic_ui_theme']['components_themes'][$section][$component]['#options'] = array(
          'default' => t("Default")
        ) + $form['semantic_ui_theme']['components_themes'][$section][$component]['#options'];

      }
    }
  }

  $form['semantic_ui_theme']['components_themes']['global']['#weight'] = 1;
  $form['semantic_ui_theme']['components_themes']['elements']['#weight'] = 2;
  $form['semantic_ui_theme']['components_themes']['collections']['#weight'] = 3;
  $form['semantic_ui_theme']['components_themes']['views']['#weight'] = 4;
  $form['semantic_ui_theme']['components_themes']['modules']['#weight'] = 5;

  $form['#submit'][] = 'semantic_plus_form_submit';
}

function semantic_plus_form_submit($form, &$form_state) {

  /**
   * Updating the theme.config from Semantic UI.
   */
  $config_path = drupal_get_path('theme', 'semantic_plus') . '/semantic/src/theme.config';
  $config = file_get_contents($config_path);

  foreach ($form['semantic_ui_themes']['#items'] as $section => $components) {
    foreach ($components as $component => $themes) {
      if ($form_state->getValue('toggle_global')) {
        if (in_array($form_state->getValue('global_theme'), $themes)) {
          $config = preg_replace('/(@' . $component . '\s*:\s*\').*\';/', '${1}' . $form_state->getValue('global_theme') . '\';', $config);
        } else {
          $config = preg_replace('/(@' . $component . '\s*:\s*\').*\';/', '${1}default\';', $config);
        }
      } else {
        $config = preg_replace('/(@' . $component . '\s*:\s*\').*\';/', '${1}' . $form_state->getValue($component) . '\';', $config);
      }
    }
  }

  file_put_contents($config_path, $config);

  /**
   * Compiling less sources from Semantic Plus Theme
   */
  require_once "less.php/Less.php";

  $less_sources = file_scan_directory(drupal_get_path('theme', 'semantic_plus') . '/css/src', '/\.less$/');

  // @TODO: Help the Less.php to compile the source properly.
  //
  // foreach ($less_sources as $key => $value) {
  //   $less_parser = new Less_Parser();
  //
  //   $less_parser->parseFile($value->uri);
  //   $less_parser->ModifyVars(array(
  //     'themesFolder' => 'themes',
  //     'siteFolder' => 'site/',
  //     'site' => $form_state->getValue('global_theme'),
  //     'theme' => $form_state->getValue('global_theme'),
  //     'type' => 'element',
  //     'element' => 'container',
  //   ));
  //
  //   kint($less_parser->getCss());
  //   // file_put_contents(str_replace(array("/src", ".less"), array("", ".css"), $value->uri, $less_parser->getCss());
  // }
}
